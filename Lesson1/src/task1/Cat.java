package task1;

public class Cat {

    private String alias;
    private String coatColor;
    private int age;
    private double weight;
    private int foodIntake;

    public Cat(String alias, String coatColor, int age, double weight) {
        this.alias = alias;
        this.coatColor = coatColor;
        this.age = age;
        this.weight = weight;
    }

    public void info() {
        System.out.println(this.alias + " is 10 years old. It weighs " + this.weight + " kg. It has a " + this.coatColor + " coat.");
    }

    public void voice() {
        switch (this.age) {
            case 1:
                System.out.println(this.alias + " yelling: Meeu");
                break;
            case 2:
                System.out.println(this.alias + " yelling: Myau");
                break;
            default:
                System.out.println(this.alias + " yelling: MEOW");
                break;
        }
    }

    public void eat() {
        foodIntake++;
        System.out.println("Cat " + this.alias + " has eaten");
        if (foodIntake == 2)
            sleep();
    }

    private void sleep() {
        System.out.println("Cat " + alias + " ate 2 times and went to sleep");
        foodIntake = 0;
    }

    public String getAlias() {
        return alias;
    }

    public String getCoatColor() {
        return coatColor;
    }

    public int getAge() {
        return age;
    }

    public double getWeight() {
        return weight;
    }
}
