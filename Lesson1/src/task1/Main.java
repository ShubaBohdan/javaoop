package task1;

public class Main {
    public static void main(String[] args) {
        Cat cat1 = new Cat("Barsick", "gray", 6, 5.5);
        Cat cat2 = new Cat("Pushock", "white", 1, 2.1);
        Cat cat3 = new Cat ("Max", "black",2, 3);

        cat1.info();
        cat2.info();
        cat3.info();

        cat1.voice();
        cat2.voice();
        cat1.eat();
        cat1.eat();
        cat2.eat();
        cat2.voice();
        cat2.eat();
        cat3.voice();
    }
}
