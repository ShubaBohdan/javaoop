package task2;

public class Main {
    public static void main(String[] args) {

        Triangle triangle1 = new Triangle(100.4, 101.3, 4.1);
        Triangle triangle2 = new Triangle(10, 23.8, 556.44);
        Triangle triangle3 = new Triangle(6.6, 4.5, 7.3);

        showCalcSquare(triangle1);
        showCalcSquare(triangle2);
        showCalcSquare(triangle3);

    }

    public static void showCalcSquare(Triangle triangle) {
        if (triangle.calculationSquare() <= 0) {
            System.out.println("This is not a triangle.");
        } else {
            System.out.println("Triangle square: " + triangle.calculationSquare());
        }
    }


}
