package task2;

public class Triangle {

    private double sideA;
    private double sideB;
    private double sideC;

    public Triangle(double a, double b, double c) {
        this.sideA = a;
        this.sideB = b;
        this.sideC = c;
    }

    public double calculationSquare() {
        if (this.sideA + this.sideB <= this.sideC ||this.sideA + this.sideC <= this.sideB ||this.sideB + this.sideC <= this.sideA) {
            return 0;
        } else {
            double p = (this.sideA + this.sideB + this.sideC) / 2;
            double inter = p * (p - this.sideA) * (p - this.sideB) * (p - this.sideC);
            return Math.sqrt(inter);
        }
    }

    public double getSideA() {
        return sideA;
    }

    public double getSideB() {
        return sideB;
    }

    public double getSideC() {
        return sideC;
    }
}


