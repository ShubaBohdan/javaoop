package task3;

public class Main {
    public static void main(String[] args) {

        Vector3d vector1 = new Vector3d(2, 4, 5, "vector A");
        Vector3d vector2 = new Vector3d(1, 2, 3, "vector B");
        Vector3d vector3 = new Vector3d(2, 1, -2, "vector C");

        System.out.println(vector1);
        System.out.println(vector2);
        System.out.println(vector3);


        System.out.println(showvVectorAddition(vector1, vector2));
        System.out.println(showScalarProductVectors(vector1, vector3));
        System.out.println(showVectorProduct(vector2, vector3));


    }

    public static String showvVectorAddition(Vector3d a, Vector3d b) {
        return "Vector addition " + a.getName() + " and " + b.getName() + " : " + a.vectorAddition(b);
    }

    public static String showScalarProductVectors(Vector3d a, Vector3d b) {
        return "Scalar product " + a.getName() + " and " + b.getName() + " : " + a.scalarProductVectors(b);
    }

    public static String showVectorProduct(Vector3d a, Vector3d b) {
        return "Vector product " + a.getName() + " and " + b.getName() + " : " + a.vectorProduct(b);
    }

}
