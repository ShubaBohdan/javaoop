package task3;

public class Vector3d {

    private String name;
    private double coordinateX;
    private double coordinateY;
    private double coordinateZ;

    public Vector3d(double x, double y, double z, String name) {
        this.coordinateX = x;
        this.coordinateY = y;
        this.coordinateZ = z;
        this.name = name;
    }

    public Vector3d vectorAddition(Vector3d b) {
        return new Vector3d(getCoordinateX() + b.getCoordinateX(),
                getCoordinateY() + b.getCoordinateY(),
                getCoordinateZ() + b.getCoordinateZ(), "D");
    }


    public double scalarProductVectors(Vector3d b) {
        return getCoordinateX() * b.getCoordinateX() + getCoordinateY() * b.getCoordinateY() + getCoordinateZ() * b.getCoordinateZ();
    }

    public Vector3d vectorProduct(Vector3d b) {
        double i = getCoordinateY() * b.getCoordinateZ() - getCoordinateZ() * b.getCoordinateY();
        double j = -(getCoordinateX() * b.getCoordinateZ() - getCoordinateZ() * b.getCoordinateX());
        double k = getCoordinateX() * b.getCoordinateY() - getCoordinateY() * b.getCoordinateX();
        return new Vector3d(i, j, k, "D");
    }



    public double getCoordinateX() {
        return coordinateX;
    }

    public double getCoordinateY() {
        return coordinateY;
    }

    public double getCoordinateZ() {
        return coordinateZ;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return getName() + " (" + getCoordinateX() +
                "; " + getCoordinateY() +
                "; " + getCoordinateZ() + ")";
    }

}
