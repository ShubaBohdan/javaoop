public class Blackboard {

    private Shape[] shapeOnBoard = new Shape[4];


    public Shape[] getShapeOnBoard() {
        return shapeOnBoard;
    }

    public void drawShape(Shape shape, int selectPartBoard) {
        if (shapeOnBoard[selectPartBoard - 1] == null)shapeOnBoard[selectPartBoard - 1] = shape;
        else System.out.println("Erase the shape from this part of the blackboard!");
    }

    public void wipeShape(int selectPartBoard) {
        shapeOnBoard[selectPartBoard - 1] = null;
    }

    public void show() {
        for (int i = 0; i < 4; i++) {
            char t;
            if (i % 2 == 0) {
                t = '\t';
            } else {
                t = '\n';
            }
            if (shapeOnBoard[i] != null) System.out.print(shapeOnBoard[i].getNameShape() + t);
            else System.out.print("nothing" + t);
        }
    }
}
