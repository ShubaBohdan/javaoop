public class Circle extends Shape {

    private String nameShape;
    private Point o;
    private Point radiusp;

    Circle(String nameShape, Point o, Point radiusp) {
        this.nameShape = nameShape;
        this.o = o;
        this.radiusp = radiusp;
    }

    private double line(Point a, Point b) {
        return Math.sqrt(Math.abs(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2)));
    }

    @Override
    double getPerimeter() {
        return 2 * Math.PI * line(o, radiusp);
    }

    @Override
    double getArea() {
        return Math.PI * Math.pow(line(o, radiusp), 2);
    }

    @Override
    String getNameShape() {
        return nameShape;
    }


    @Override
    public String toString() {
        return nameShape + "\nCoordinates of points:\n" +
                o.toString() + '\n' +
                 "Perimetr " + getPerimeter() + '\n' + "Area " + getArea() + '\n';
    }
}
