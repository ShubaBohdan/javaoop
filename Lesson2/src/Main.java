public class Main {
    public static void main(String[] args) {

        Blackboard blackboard = new Blackboard();


        Circle circle = new Circle("Circle",
                new Point("O", 5.5, 5.5),
                new Point("R", 7, 7.5));
        Rectangle rectangle = new Rectangle("Rectangle",
                new Point("A", 1, 1),
                new Point("B", 1, 4),
                new Point("C", 6, 4),
                new Point("D", 6, 1));
        Rhombus rhombus = new Rhombus("Rhombus",
                new Point("A", 3, 2.5),
                new Point("B", 5, 4),
                new Point("C", 7, 2.5),
                new Point("D", 5, 1));
        Square square = new Square("Square",
                new Point("A", 2, 3),
                new Point("B", 2, 6),
                new Point("C", 5, 6),
                new Point("D", 5, 3));


        System.out.println(circle);
        System.out.println(rectangle);
        System.out.println(rhombus);
        System.out.println(square);


        blackboard.drawShape(rhombus, 1);
        blackboard.drawShape(square, 2);
        blackboard.drawShape(square, 3);
        blackboard.drawShape(circle, 4);
        blackboard.show();
        System.out.println("The total area of the shape on the blackboard: " + areaShapeOnBoard(blackboard.getShapeOnBoard()) + '\n');


        blackboard.drawShape(square, 4);
        System.out.println();


        blackboard.wipeShape(2);
        blackboard.wipeShape(3);
        blackboard.wipeShape(4);

        Rhombus rhombus2 = new Rhombus("Rhombus2",
                new Point("A", 2, 2.5),
                new Point("B", 5, 4),
                new Point("C", 8, 2.5),
                new Point("D", 5, 1));

        Rhombus rhombus3 = new Rhombus("Rhombus3",
                new Point("A", 4, 4.5),
                new Point("B", 6, 4),
                new Point("C", 8, 3),
                new Point("D", 6, 1.5));

        blackboard.drawShape(rhombus2, 2);
        blackboard.drawShape(rhombus, 3);
        blackboard.drawShape(rhombus3, 4);
        blackboard.show();
        System.out.println("The total area of the shape on the blackboard: " + areaShapeOnBoard(blackboard.getShapeOnBoard()));

    }

    public static double areaShapeOnBoard(Shape[] shapeOnBoard) {
        double calc = 0;
        for (int i = 0; i < 4; i++) {
            if (shapeOnBoard[i] != null) calc += shapeOnBoard[i].getArea();
        }
        return calc;
    }
}
