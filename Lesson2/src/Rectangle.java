public class Rectangle extends Shape {

    private String nameShape;
    private Point a;
    private Point b;
    private Point c;
    private Point d;

    Rectangle(String nameShape, Point a, Point b, Point c, Point d) {
        this.nameShape = nameShape;
        this.a = a;
        this.b = b;
        this.c = c;
        this.d = d;
    }

    private double line(Point a, Point b) {
        return Math.sqrt(Math.abs(Math.pow((a.getX() - b.getX()), 2) + Math.pow((a.getY() - b.getY()), 2)));
    }

    @Override
    double getPerimeter() {
        return (line(a, b) + line(a, d)) * 2;
    }

    @Override
    double getArea() {
        return line(a, b) * line(a, d);
    }

    @Override
    String getNameShape() {
        return nameShape;
    }


    @Override
    public String toString() {
        return nameShape + "\nCoordinates of points:\n" +
                a.toString() + '\n' + b.toString() + '\n' + c.toString() + '\n' + d.toString() +
                '\n' + "Perimetr " + getPerimeter() + '\n' + "Area " + getArea() + '\n';
    }
}

