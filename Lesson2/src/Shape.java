public abstract class Shape {

    abstract double getPerimeter();
    abstract double getArea();
    abstract String getNameShape();



}
