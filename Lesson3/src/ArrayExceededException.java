public class ArrayExceededException extends Exception {
    public ArrayExceededException(String message) {
        super(message);
    }

}
