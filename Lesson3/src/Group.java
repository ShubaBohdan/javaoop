
import java.util.ArrayList;
import java.util.Collections;

public class Group {

    private ArrayList<Student> listGroup = new ArrayList<Student>();

    public void addToGroup(Student student) throws ArrayExceededException {
        if (listGroup.size() >= 10) {
            throw new ArrayExceededException("ArrayExceededException: Group clogged");
        } else listGroup.add(student);

    }

    public void removeStudent(Student student) {
        listGroup.remove(student);
    }

    public Student search(String lastName) throws Exception {
        for (int i = 0; i < listGroup.size(); i++) {
            if (lastName.equals(listGroup.get(i).getLastName())) {
                return listGroup.get(i);
            }

        }
        throw new Exception();
    }

    @Override
    public String toString() {
        Collections.sort(listGroup);
        String str = "";
        for (int i = 0; i < listGroup.size(); i++) {
            str = str + (i + 1) + ". " + listGroup.get(i).getLastName() + " " + listGroup.get(i).getName() + '\n';

        }
        return str;
    }
}


