import java.util.Objects;

public class Human {

    private String name;
    private String lastName;
    private int yearBirth;
    private String gender;

    public Human(String name, String lastName, int yearBirth, String gender) {
        this.name = name;
        this.lastName = lastName;
        this.yearBirth = yearBirth;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public String getLastName() {
        return lastName;
    }

    public int getYearBirth() {
        return yearBirth;
    }

    public String getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return this.name + ' ' + this.lastName + '\'' +
                " (born in " + this.yearBirth +
                "; gender: " + this.gender + ")";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return yearBirth == human.yearBirth &&
                Objects.equals(name, human.name) &&
                Objects.equals(lastName, human.lastName) &&
                Objects.equals(gender, human.gender);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, lastName, yearBirth, gender);
    }
}
