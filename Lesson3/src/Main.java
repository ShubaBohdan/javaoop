public class Main {
    public static void main(String[] args) {

        Group group = new Group();

        Student student1 = new Student("Grishanya", "Kamenev", 1982, "male", 77);
        Student student2 = new Student("Misha", "Stol", 1983, "male", 75);
        Student student3 = new Student("Roma", "Nema", 1986, "male", 97);
        Student student4 = new Student("Lessy", "Pervol", 1989, "female,", 77);
        Student student5 = new Student("Galya", "Richka", 1991, "female", 67);
        Student student6 = new Student("Bill", "Blinov", 1988, "male", 77);
        Student student7 = new Student("George", "Linsky", 1979, "male", 65);
        Student student8 = new Student("Lena", "Golovach", 1982, "female,", 88);
        Student student9 = new Student("Linux", "Ubuntovich", 1992, "male", 90);
        Student student10 = new Student("Julia", "Itachova", 1982, "female,", 76);
        Student student11 = new Student("Gerry", "Gora", 1982, "male", 77);
        Student student12 = new Student("Lindsay", "Black", 1982, "female,", 81);

        System.out.println(student5.toString());

        try {
            group.addToGroup(student1);
            group.addToGroup(student2);
            group.addToGroup(student3);
            group.addToGroup(student4);
            group.addToGroup(student5);
            group.addToGroup(student6);
            group.addToGroup(student7);
            group.addToGroup(student8);
            group.addToGroup(student9);
            group.addToGroup(student10);
            group.addToGroup(student11);
            group.addToGroup(student12);
        } catch (ArrayExceededException e) {
            System.out.println(e.getMessage());
        }

        group.removeStudent(student4);

        try {
            group.addToGroup(student12);
        } catch (ArrayExceededException e) {
            System.out.println(e.getMessage());
        }


        try {
            System.out.println(group.search("Stol"));
        } catch (Exception e) {
            System.out.println("There is no student with this last name in the group.");
        }



        System.out.println(group.toString());

    }
}
