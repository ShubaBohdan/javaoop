import java.util.Objects;

public class Student extends Human implements Comparable {

    private double gradPointAverage;

    public Student(String name, String lastName, int yearBirth, String gender, double gradPointAverage) {
        super(name, lastName, yearBirth, gender);
        this.gradPointAverage = gradPointAverage;
    }


    public double getGradPointAverage() {
        return gradPointAverage;
    }

    @Override
    public String getName() {
        return super.getName();
    }

    @Override
    public String getLastName() {
        return super.getLastName();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Student student = (Student) o;
        return Double.compare(student.gradPointAverage, gradPointAverage) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), gradPointAverage);
    }

    @Override
    public int compareTo(Object o) {
        if (o == null) {
            return -1;
        }
        Student student = (Student) o;
        return super.getLastName().compareToIgnoreCase(student.getLastName());
    }

    @Override
    public String toString() {

        return getLastName() + ' ' + getName() +
                " (born in " + getYearBirth() +
                "; gender: " + getGender() + ") GPA: " + this.gradPointAverage;
    }
}
